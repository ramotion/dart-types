# foreign-types

This library generates foreign types (together with Aeson-generic compatible JSON encoding/decoding) from Haskell types via GHC Generics.

Example usage:

```haskell
data Sex = Male | Female deriving (Generic,ForeignType,ToJSON,FromJSON)

data User = User {
    name :: Text
  , age :: Maybe Int
  , sex :: Maybe Sex
  , married :: Bool
  } deriving (Generic,ForeignType,ToJSON,FromJSON)

-- generated Dart code:
dartCode :: Text
dartCode = renderDeps @User Dart
```

Barbie-style records should also work, although if you do so for multiple choices of functor, you'll need to explicitly assign names to avoid collisions in the generated foreign code:

```haskell
data User f = User {
    name :: f Text
  , age :: f (Maybe Int)
  , sex :: f (Maybe Sex)
  , married :: f Bool
  } deriving (Generic)

instance (forall a . AE.ToJSON a => AE.ToJSON (f a)) => AE.ToJSON (User f)
instance (forall a . AE.FromJSON a => AE.FromJSON (f a)) => AE.FromJSON (User f)
instance ForeignType (User (Const Bool)) where
  typeName = "UserBool"
instance ForeignType (User []) where
  typeName = "UserList"
instance ForeignType (User Identity)

dartCode :: Text
dartCode = renderDeps (User @(Const Bool) , User @Identity , User @[]) Dart
```
