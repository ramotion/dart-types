{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

module Foreign.RPC.HTTP where

import Data.Coerce
import Data.Kind (Constraint, Type)
import Data.Proxy
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.Read as TR
import qualified Data.Text.Lazy as LT
import qualified Data.Text.Lazy.Builder as TB
import Data.Text.Lazy.Builder.Int (decimal)
import GHC.Generics (Generic)
import GHC.TypeLits (KnownSymbol,Symbol,symbolVal)
import GHC.TypeError

import ForeignType


data Verb a where
  GET :: Verb ()
  POST :: Verb a
  PUT :: Verb a
  DELETE :: Verb ()

data SAPI (name :: Symbol) (verb :: Verb body) (query :: [ Type ]) (result :: Type)

type family Tup (xs :: [ Type ]) where
  Tup '[] = ()
  Tup '[ a ] = a
  Tup '[ a , b ] = (a , b)
  Tup '[ a , b , c ] = (a , b , c)

type VBody :: forall (a :: Type) . Verb a -> Type
type family VBody (v :: Verb a) where
  VBody @a v = a

type family Sig (s :: Type) where
  Sig (SAPI n v q r) = Tup q -> VBody v -> IO r
  Sig _ = TypeError ('Text "SAPI Sig Nonsense (expected SAPI)")


type family SAPIDeps (s :: Type) :: [ Type ] where
  SAPIDeps (SAPI n GET q r) = r ': q
  SAPIDeps (SAPI n DELETE q r) = r ': q
  SAPIDeps (SAPI n v q r) = VBody v ': r ': q
  SAPIDeps _ = TypeError ('Text "SAPI Deps Nonsense (expected SAPI)")


-- extremely annoying that this is necessary, due to lack of ability to lambda bind type variables
-- in Endpoint pattern matches without it
data Retain (n :: Symbol) (v :: Verb vb) (g :: [ Type ]) (r :: Type) = Retain
data APIImpl a where
  Endpoint :: forall n v g r . Retain n v g r -> Sig (SAPI n v g r) -> APIImpl (SAPI n v g r)

infixr 5 :|

data Endpoints (xs :: [ Type ]) where
  End :: Endpoints '[]
  (:|) :: APIImpl x -> Endpoints xs -> Endpoints (x ': xs)

type family EDeps (xs :: [ Type ]) :: Type where
  EDeps '[] = ()
  EDeps (y ': ys) = (EDeps' (SAPIDeps y) , EDeps ys)

type family EDeps' (xs :: [ Type ]) :: Type where
  EDeps' '[] = ()
  EDeps' (y ': ys) = (y , EDeps' ys)

type family CUnlist (c :: Type -> Constraint) (xs :: [ Type ]) :: Constraint where
  CUnlist c '[] = ()
  CUnlist c (x ': xs) = (c x , CUnlist c xs)

type family ECons (vc :: Type -> Constraint) (gc :: Type -> Constraint) (rc :: Type -> Constraint) (xs :: [ Type ]) :: Constraint where
  ECons vc gc rc '[] = ()
  ECons vc gc rc (SAPI n v g r ': xs) = (vc (VBody v) , CUnlist gc g , rc r , ECons vc gc rc xs)

class QueryParams a where
  queryCount :: Int
  encodeQueryParams :: a -> [ Text ]
  decodeQueryParams :: [ Text ] -> Either String a
  queryParamNames :: ForeignLanguage -> [ (Text , Text) ] -- parameter name, type name, e.g., `preferred : Sex`

instance (QueryParams a , QueryParams b) => QueryParams (a , b) where
  queryCount = queryCount @a + queryCount @b
  encodeQueryParams (x , y) = encodeQueryParams x <> encodeQueryParams y
  decodeQueryParams xs = (,) <$> decodeQueryParams (take (queryCount @a) xs)
                             <*> decodeQueryParams (drop (queryCount @a) xs)
  queryParamNames l = queryParamNames @a l <> queryParamNames @b l

newtype NamedQueryParam (n :: Symbol) a = NamedQueryParam a
  deriving (Generic,Eq,Ord,Show)

instance KnownSymbol n => QueryParams (NamedQueryParam n Text) where
  queryCount = 1
  encodeQueryParams = pure . coerce
  decodeQueryParams [ x ] = pure (coerce x)
  decodeQueryParams _ = Left "decodeQueryParams: impossible"
  queryParamNames l = [(T.pack (symbolVal (Proxy :: Proxy n)) , typeName @Text l)]

instance KnownSymbol n => QueryParams (NamedQueryParam n Int) where
  queryCount = 1
  encodeQueryParams (NamedQueryParam i) = [ LT.toStrict (TB.toLazyText (decimal i)) ]
  decodeQueryParams [ x ] = case TR.decimal x of
    Left e -> Left e
    Right (i,_) -> Right (NamedQueryParam i)
  decodeQueryParams _ = Left "decodeQueryParams: impossible"
  queryParamNames l = [(T.pack (symbolVal (Proxy :: Proxy n)) , typeName @Int l)]

instance KnownSymbol n => QueryParams (NamedQueryParam n Word) where
  queryCount = 1
  encodeQueryParams (NamedQueryParam i) = [ LT.toStrict (TB.toLazyText (decimal i)) ]
  decodeQueryParams [ x ] = case TR.decimal x of
    Left e -> Left e
    Right (i,_) -> Right (NamedQueryParam i)
  decodeQueryParams _ = Left "decodeQueryParams: impossible"
  queryParamNames l = [(T.pack (symbolVal (Proxy :: Proxy n)) , typeName @Word l)]

efoldMap :: forall (vc :: Type -> Constraint)
                   (gc :: Type -> Constraint)
                   (rc :: Type -> Constraint)
                   (xs :: [ Type ])
                   (a :: Type)
         . (ECons vc gc rc xs , Monoid a)
         => (forall vb n (v :: Verb vb) g r . (vc vb , CUnlist gc g , rc r) => Retain n v g r -> a) -> Endpoints xs -> a
efoldMap _ End = mempty
efoldMap f (Endpoint x _ :| xs) = f x <> efoldMap @vc @gc @rc @_ @a f xs

foreignEndpointSnippet :: ECons ForeignType QueryParam ForeignType xs
                       => ForeignLanguage -> Endpoints xs -> Text
foreignEndpointSnippet l = efoldMap @ForeignType @QueryParam @ForeignType f
  where
    {-# INLINE f #-}
    f :: forall vb n (v :: Verb vb) qs r . (ForeignType vb , CUnlist QueryParam qs , ForeignType r)
      => Retain n v qs r -> Text
    f _ = _

-- type dependencies of endpoints to render foreign language client bindings
edeps :: forall (xs :: [ Type ]) . ForeignType (EDeps xs) => Endpoints xs -> ForeignLanguage -> Text
edeps _ = renderDeps @(EDeps xs)


