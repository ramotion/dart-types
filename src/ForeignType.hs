{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE NoFieldSelectors #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE UndecidableInstances #-}

-- | This library generates foreign types (together with Aeson-generic compatible JSON encoding/decoding) from Haskell types via GHC Generics.
--
-- Example usage:
-- 
-- @
-- data Sex = Male | Female deriving (Generic,ForeignType,ToJSON,FromJSON)
-- 
-- data User = User {
--     name :: Text
--   , age :: Maybe Int
--   , sex :: Maybe Sex
--   , married :: Bool
--   } deriving (Generic,ForeignType,ToJSON,FromJSON)
--
-- -- generated Swift code:
-- dartCode :: Text
-- dartCode = renderDeps @User Dart
-- @
--
-- Barbie-style records should also work, although if you do so for multiple choices of functor, you'll need to explicitly assign names to avoid collisions in the generated foreign code:
--
-- @
-- data User f = User {
--     name :: f Text
--   , age :: f (Maybe Int)
--   , sex :: f (Maybe Sex)
--   , married :: f Bool
--   } deriving (Generic)
-- 
-- instance (forall a . AE.ToJSON a => AE.ToJSON (f a)) => AE.ToJSON (User f)
-- instance (forall a . AE.FromJSON a => AE.FromJSON (f a)) => AE.FromJSON (User f)
-- instance ForeignType (User (Const Bool)) where
--   typeName = "UserBool"
-- instance ForeignType (User []) where
--   typeName = "UserList"
-- instance ForeignType (User Identity)
--
-- dartCode :: Text
-- dartCode = renderDeps (User @(Const Bool) , User @Identity , User @[]) Dart
--
-- @

module ForeignType (ForeignType(typeName,jsEncode,jsDecode) , ForeignLanguage(..) , renderDeps) where

import Data.DList
import Data.Functor
import Data.Functor.Identity
import Data.Functor.Const
import qualified Data.Map as M
import Data.Maybe
import Data.Proxy
import Data.Text (Text)
import qualified Data.Text as T
import GHC.Generics
import GHC.Int
import GHC.TypeLits
import GHC.Word


type TypeName = Text
type Declaration = Text

data ForeignLanguage = Dart | Swift

class ForeignType a where
  -- includes self
  dependencies :: ForeignLanguage -> M.Map TypeName (Maybe Declaration)
  default dependencies :: ( GDartType a (Rep a)
                          , GSwiftType a (Rep a)
                          ) => ForeignLanguage -> M.Map TypeName (Maybe Declaration)
  dependencies Dart = gdependenciesDart @a @(Rep a) (typeName @a Dart)
  dependencies Swift = gdependenciesSwift @a @(Rep a) (typeName @a Swift)
  
  typeName :: ForeignLanguage -> TypeName
  default typeName :: ( GDartType a (Rep a)
                      , GSwiftType a (Rep a)
                      ) => ForeignLanguage -> TypeName
  typeName Dart = gtypeNameDart @a @(Rep a)
  typeName Swift = gtypeNameSwift @a @(Rep a)
  
  declaration :: ForeignLanguage -> Maybe Declaration
  default declaration :: ( GDartType a (Rep a)
                         , GSwiftType a (Rep a)
                         ) => ForeignLanguage -> Maybe Declaration
  declaration Dart = gdeclarationDart @a @(Rep a) (typeName @a Dart)
  declaration Swift = gdeclarationSwift @a @(Rep a) (typeName @a Swift)
  
  jsEncode :: ForeignLanguage -> Maybe Text
  default jsEncode :: ( GDartType a (Rep a)
                      , GSwiftType a (Rep a)
                      ) => ForeignLanguage -> Maybe Text
  jsEncode Dart = gjsEncodeDart @a @(Rep a) (typeName @a Dart)
  jsEncode Swift = gjsEncodeSwift @a @(Rep a) (typeName @a Swift)

  jsDecode :: ForeignLanguage -> Maybe Text
  default jsDecode :: ( GDartType a (Rep a)
                      , GSwiftType a (Rep a)
                      ) => ForeignLanguage -> Maybe Text
  jsDecode Dart = gjsDecodeDart @a @(Rep a) (typeName @a Dart)
  jsDecode Swift = gjsDecodeSwift @a @(Rep a) (typeName @a Swift)

type FieldName = Text

data FlatType = FlatType {
    dependencies :: M.Map TypeName (Maybe Declaration)
  , typeName :: TypeName
  , jsEncode :: Maybe Text
  , jsDecode :: Maybe Text
  }

recordOrTuple :: DList (Maybe FieldName , FlatType) -> Either [(FieldName,FlatType)]
                                                                  [ FlatType ]
recordOrTuple (Data.DList.toList -> xs) = case xs of
  [] -> Right []
  ((Nothing,_):_) -> pure do snd <$> xs
  ((Just _,_):_) -> Left do xs <&> \(m,x) -> case m of
                                Just n -> (n , x)
                                Nothing -> error "recordOrTuple: impossible"


isSimpleEnum :: [ (Text , DList a) ] -> Maybe [ Text ]
isSimpleEnum xs = traverse (\(n,fs) -> if null (Data.DList.toList fs) then Just n else Nothing) xs

type ConstructorName = Text



-- render the text of a dart file declaring this type any necessary dependencies
renderDeps :: forall a. ForeignType a => ForeignLanguage -> Text
renderDeps Dart = do
  T.concat $
      do "import 'dart:convert';\n\n"
    : do maybe "" ((<>"\n\n")) . snd <$>
           M.toAscList do dependencies @a Dart
renderDeps Swift = do
  T.concat $
      do "import Foundation;\n\n"
    : do maybe "" ((<>"\n\n")) . snd <$>
           M.toAscList do dependencies @a Swift


-----------------------
-----    DART    ------
-----------------------

class GDartType s f where
  gdependenciesDart :: TypeName -> M.Map TypeName (Maybe Declaration)
  gdeclarationDart :: TypeName -> Maybe Declaration
  gtypeNameDart :: TypeName
  gjsEncodeDart :: TypeName -> Maybe Text
  gjsDecodeDart :: TypeName -> Maybe Text

-- newtypes
instance (ForeignType a , KnownSymbol n , g ~ (D1 (MetaData n x y True) (C1 w (S1 l (Rec0 a))))) => GDartType s (D1 (MetaData n x y True) (C1 w (S1 l (Rec0 a)))) where
  gdependenciesDart n =
    M.insert do n
             do gdeclarationDart @s @g n
             do dependencies @a Dart
  gdeclarationDart n = Just $ T.concat [
      "class " <> n <> " {\n"
    , "    " <> typeName @a Dart <> " rawValue;\n"
    , "    " <> n <> "(this.rawValue);\n"
    , "}"
    ]
  gtypeNameDart = T.pack (symbolVal (Proxy :: Proxy n))
  gjsEncodeDart _ = Just case jsEncode @a Dart of
    Just e -> "((x) => " <> e <> "(x.rawValue))"
    Nothing -> "((x) => x.rawValue)"
  gjsDecodeDart n = Just case jsDecode @a Dart of
    Just d -> "((x) => " <> n <> "(" <> d <> "(x) as " <> typeName @a Dart <> "))"
    Nothing -> "((x) => " <> n <> "(x as " <> typeName @a Dart <> "))"
  
-- records / tuples
instance (KnownSymbol n , GDartProduct s f , g ~ D1 (MetaData n x y False) (C1 w f)) => GDartType s (D1 (MetaData n x y False) (C1 w f)) where
  gtypeNameDart = case recordOrTuple (rfieldsDart @s @f) of
    Left _ -> T.pack (symbolVal (Proxy :: Proxy n))
    Right xs -> "(" <> T.intercalate "," ((\x -> x.typeName) <$> xs) <> ")"
  gdependenciesDart n =
    M.insert do n
             do gdeclarationDart @s @g n
             do foldMap (\(_,f) -> f.dependencies) $ toList (rfieldsDart @s @f)
  gdeclarationDart cname = case recordOrTuple (rfieldsDart @s @f) of
    Right _ -> Nothing
    Left fs -> Just do
      "class " <> cname <> " {\n"
        <> do flip foldMap fs \(n,f) -> "    " <> f.typeName <> " " <> n <> ";\n"
        <> "\n"

           -- constructor
        <> do "    " <> cname <> "(" <> T.intercalate "," ((\(n,_) -> "this." <> n) <$> fs) <> ");\n"

           -- fromJSON
        <> do "    " <> cname <> ".fromJson(Map<String,dynamic> json) :"
                 <> do T.intercalate ","
                         do fs <&> \(n,f) -> " " <> n <> " = " <> case f.jsDecode of
                                    Just d -> d <> "(json['" <> n <> "'])"
                                    Nothing -> "json['" <> n <> "']"
                 <> do ";\n"

           -- toJSON
        <> do "    Map<String, dynamic> toJson() {"
                 <> do "return {"
                         <> do T.intercalate ","
                                 do fs <&> \(n,f) -> case f.jsEncode of
                                     Just e -> "'" <> n <> "':" <> e <> "(" <> n <> ")"
                                     Nothing -> "'" <> n <> "':" <> n
                         <> do "};"
                 <> do "}\n"
        <> "}"
  
  gjsEncodeDart n = case recordOrTuple (rfieldsDart @s @f) of
    Right [] -> Just "((_) => null)"
    Right [ x ] -> x.jsEncode
    Right xs -> Just do
      let ixs = zip [ (1 :: Int) .. ] xs
          es = ixs <&> \(i,x) -> case x.jsEncode of
                                   Just e -> e <> "(t.$" <> T.pack (show i) <> ") as dynamic"
                                   Nothing -> "t.$" <> T.pack (show i) <> " as dynamic"
      "((t) => [" <> T.intercalate "," es <> "])"
    Left _ -> Just do "((x) => (x as " <> n <> ").toJson())"
  gjsDecodeDart n = case recordOrTuple (rfieldsDart @s @f) of
    Right [] -> Just "((_) => ())"
    Right [ x ] -> x.jsDecode
    Right xs -> Just do
      let ixs = zip [ (0 :: Int) .. ] xs
          es = ixs <&> \(i,x) -> case x.jsDecode of
                                   Just d -> d <> "(xs[" <> T.pack (show i) <> "])"
                                   Nothing -> "xs[" <> T.pack (show i) <> "] as " <> x.typeName
      "((xs) => (" <> T.intercalate "," es <> "))"
    Left _ -> Just do "((x) => " <> n <> ".fromJson(x as Map<String,dynamic>))"

-- sums
instance (KnownSymbol n , GDartEnum s (f :+: g) , h ~ D1 (MetaData n x y False) (f :+: g)) => GDartType s (D1 (MetaData n x y False) (f :+: g)) where
  gtypeNameDart = T.pack (symbolVal (Proxy :: Proxy n))
  gdependenciesDart n =
    M.insert do n
             do gdeclarationDart @s @h n
             do foldMap
                  do \(_,fs) -> foldMap (\(_,f) -> f.dependencies) fs
                  do toList (gEnumBodyDart @s @(f :+: g))
  gdeclarationDart cname = do
    let ss = toList (gEnumBodyDart @s @(f :+: g))
    case isSimpleEnum ss of
      Just es -> Just do
        "enum " <> cname <> " {\n  " <> T.intercalate ",\n  " es <> "\n}\n"
      Nothing -> Just do
        "class " <> cname <> " {\n"
          <> do "    // constructors\n"
             -- constructors. a bit weird, since we have multiple here and we're required to choose a
             -- default, so... just name all of them, then duplicate the first one as the unnamed default.
          <> do flip foldMap ss \(cn,mfs) -> case recordOrTuple mfs of
                  Left fs -> "    " <> cname <> "." <> cn <> "("
                    <> do T.intercalate "," $ fs <&> \(f,t) ->
                            t.typeName <> " " <> f
                    <> do "): _rep = {"
                    <> do T.intercalate "," $ (:) ("\"tag\":\"" <> cn <> "\"") $
                            fs <&> \(f,t) -> "\"" <> f <> "\":" <> case t.jsEncode of
                              Just e -> e <> "(" <> f <> ") as dynamic"
                              Nothing -> f <> " as dynamic"
                    <> do "};\n"
                  Right ts -> "    " <> cname <> "." <> cn <> "("
                    <> do T.intercalate "," $ zip [ 0 :: Int .. ] ts <&> \(i,t) ->
                            t.typeName <> " v" <> T.pack (show i)
                    <> do case ts of
                            [ t ] -> "): _rep = {\"tag\":\"" <> cn <> "\",\"contents\":"
                              <> case t.jsEncode of
                                   Just e -> e <> "(v0) as dynamic"
                                   Nothing -> "v0 as dynamic"
                            _ -> "): _rep = {\"tag\":\"" <> cn <> "\",\"contents\":["
                              <> do T.intercalate "," $ 
                                      zip [ 0 :: Int .. ] ts <&> \(i,t) -> case t.jsEncode of
                                        Just e -> e <> "(v" <> T.pack (show i) <> ") as dynamic"
                                        Nothing -> "v" <> T.pack (show i) <> " as dynamic"
                              <> do "]"
                    <> do "};\n"

          <> do "\n"
             -- type-safe pattern matching via church-style encoding
          <> do "    // pattern match, guaranteed to call exactly 1 of the two provided callbacks\n"
          <> do "    T match<T>("
          <> do T.intercalate "," $
                  ss <&> \(cn,mfs) -> "T Function("
                            <> do T.intercalate "," $ toList mfs <&> \(_,t) -> t.typeName
                            <> do ") " <> cn
          <> do "){switch(_rep[\"tag\"]){"
          <> do flip foldMap ss \(cn,mfs) -> "case \"" <> cn <> "\":{return " <> cn <> "("
                   <> do T.intercalate "," case recordOrTuple mfs of
                           Left fs -> fs <&> \(f,t) -> case t.jsDecode of
                             Just d ->
                               d <> "(_rep[\"" <> f <> "\"])"
                             Nothing -> "_rep[\"" <> f <> "\"]"
                           Right [ t ] -> pure case t.jsDecode of
                             Just d -> d <> "(_rep[\"contents\"])"
                             Nothing -> "_rep[\"contents\"]"
                           Right ts -> zip [ 0 :: Int .. ] ts <&> \(i,t) -> case t.jsDecode of
                             Just d -> d <> "((_rep[\"contents\"] as List<dynamic>)[" <> T.pack (show i) <> "])"
                             Nothing -> "((_rep[\"contents\"] as List<dynamic>)[" <> T.pack (show i) <> "])"
                   <> do ");} break;"
          <> do "default:throw(\"" <> cname <> ": invalid JSON\");}}\n\n"

          <> do "    // private internals\n"
          <> do "    final Map<String,dynamic> _rep;\n"
          <> do "    " <> cname <> "._(this._rep);\n"
          -- fromJSON
          <> do "    " <> cname <> ".fromJson(Map<String,dynamic> json) : _rep = json;\n"
             -- toJSON
          <> do "    Map<String, dynamic> toJson() { return _rep; }\n"
          <> "}"
  gjsEncodeDart n = do
    let ss = toList (gEnumBodyDart @s @(f :+: g))
    case isSimpleEnum ss of
      Just _ ->
        Just do "((x) => (x as " <> n <> ").name as dynamic)" -- yes, this `as` is necessary lol
      Nothing -> Just do "((x) => (x as " <> n <> ").toJson())"
  gjsDecodeDart n = do
    let ss = toList (gEnumBodyDart @s @(f :+: g))
    case isSimpleEnum ss of
      Just _ -> Just do "((x) => " <> n <> ".values.byName(x as String))"
      Nothing -> Just do "((x) => " <> n <> ".fromJson(x as Map<String,dynamic>))"



class GDartProduct s f where
  rfieldsDart :: DList (Maybe FieldName , FlatType)

instance (KnownSymbol n , ForeignType b) => GDartProduct s (S1 (MetaSel (Just n) x y z) (K1 i b)) where
  rfieldsDart = pure $ (,) (Just (T.pack (symbolVal (Proxy :: Proxy n)))) do 
    FlatType do dependencies @b Dart
             do typeName @b Dart
             do jsEncode @b Dart
             do jsDecode @b Dart

instance ForeignType b => GDartProduct s (S1 (MetaSel Nothing x y z) (K1 i b)) where
  rfieldsDart = pure $ (,) Nothing do 
    FlatType do dependencies @b Dart
             do typeName @b Dart
             do jsEncode @b Dart
             do jsDecode @b Dart

instance (GDartProduct s f , GDartProduct s g) => GDartProduct s (f :*: g) where
  rfieldsDart = rfieldsDart @s @f <> rfieldsDart @s @g


instance GDartProduct s f => GDartProduct s (D1 x f) where
  rfieldsDart = rfieldsDart @s @f

instance GDartProduct s f => GDartProduct s (C1 (MetaCons x y z) f) where
  rfieldsDart = rfieldsDart @s @f



class GDartEnum s f where
  gEnumBodyDart :: DList (ConstructorName , DList (Maybe FieldName , FlatType))

instance {-# OVERLAPPING #-} KnownSymbol n => GDartEnum s (C1 (MetaCons n x y) U1) where
  gEnumBodyDart = pure (T.pack (symbolVal (Proxy :: Proxy n)) , mempty)

instance (KnownSymbol n , GDartProduct s f) => GDartEnum s (C1 (MetaCons n x y) f) where
  gEnumBodyDart = pure (T.pack (symbolVal (Proxy :: Proxy n)) , rfieldsDart @s @f)

instance (GDartEnum s f , GDartEnum s g) => GDartEnum s (f :+: g) where
  gEnumBodyDart = gEnumBodyDart @s @f <> gEnumBodyDart @s @g



------------------------
-----    SWIFT    ------
------------------------

class GSwiftType s f where
  gdependenciesSwift :: TypeName -> M.Map TypeName (Maybe Declaration)
  gdeclarationSwift :: TypeName -> Maybe Declaration
  gtypeNameSwift :: TypeName
  gjsEncodeSwift :: TypeName -> Maybe Text
  gjsDecodeSwift :: TypeName -> Maybe Text

-- newtypes
instance (ForeignType a , KnownSymbol n , g ~ (D1 (MetaData n x y True) (C1 w (S1 l (Rec0 a))))) => GSwiftType s (D1 (MetaData n x y True) (C1 w (S1 l (Rec0 a)))) where
  gdependenciesSwift n =
    M.insert do n
             do gdeclarationSwift @s @g n
             do dependencies @a Swift
  gdeclarationSwift n = Just $ "typealias " <> n <> " = " <> typeName @a Swift <> ";\n"
  gtypeNameSwift = T.pack (symbolVal (Proxy :: Proxy n))
  gjsEncodeSwift _ = Nothing
  gjsDecodeSwift _ = Nothing
  
-- records / tuples
-- TODO?: tuples aren't actually supported here, but it's probably not worth it anyway
instance (KnownSymbol n , GSwiftProduct s f , g ~ D1 (MetaData n x y False) (C1 w f)) => GSwiftType s (D1 (MetaData n x y False) (C1 w f)) where
  gtypeNameSwift = case recordOrTuple (rfieldsSwift @s @f) of
    Left _ -> T.pack (symbolVal (Proxy :: Proxy n))
    Right xs -> "(" <> T.intercalate "," ((\x -> x.typeName) <$> xs) <> ")"
  gdependenciesSwift n =
    M.insert do n
             do gdeclarationSwift @s @g n
             do foldMap (\(_,f) -> f.dependencies) $ toList (rfieldsSwift @s @f)
  gdeclarationSwift cname = case recordOrTuple (rfieldsSwift @s @f) of
    Right _ -> Nothing
    Left fs -> Just do
      "struct " <> cname <> " : Codable , Hashable {\n"
        <> do flip foldMap fs \(n,f) -> "    public let " <> n <> " : " <> f.typeName <> ";\n"
        <> "}"
  
  gjsEncodeSwift _ = Nothing
  gjsDecodeSwift _ = Nothing

-- sums
instance (KnownSymbol n , GSwiftEnum s (f :+: g) , h ~ D1 (MetaData n x y False) (f :+: g)) => GSwiftType s (D1 (MetaData n x y False) (f :+: g)) where
  gtypeNameSwift = T.pack (symbolVal (Proxy :: Proxy n))
  gdependenciesSwift n =
    M.insert do n
             do gdeclarationSwift @s @h n
             do foldMap
                  do \(_,fs) -> foldMap (\(_,f) -> f.dependencies) fs
                  do toList (gEnumBodySwift @s @(f :+: g))
  gdeclarationSwift cname = do
    let ss = toList (gEnumBodySwift @s @(f :+: g))
    case isSimpleEnum ss of
      Just es -> Just do
        "enum " <> cname <> " : String , Codable , Hashable {\n    "
          <> do T.intercalate "\n    " $ es <&> \e -> "case " <> e
          <> do "\n}"
      Nothing -> Just do
        T.unlines [
            "enum " <> cname <> " : Codable , Hashable {"
          , T.unlines $ ss <&> \(cn,(toList -> fs)) -> do
              if null fs
                 then "    case " <> cn
                 else "    case " <> cn
                        <> do "("
                        <> do T.intercalate "," $ fs <&> \(_,ft) -> ft.typeName
                        <> do ")"
          , ""
          , ""
          , "    public init(from decoder: Decoder) throws {"
          , "        let container = try decoder.container(keyedBy: Keys.self)"
          , "        let tag = try container.decode(Tag.self, forKey: .tag)"
          , "        switch tag {"
          , T.unlines $ ss <&> \(cn,(toList -> fs)) -> T.unlines [
                "        case ." <> cn <> ":"
              , case fs of
                  [] -> "            self = ." <> cn
                  [ (_,f) ] -> T.unlines [
                      "            let v0 = try container.decode(" <> f.typeName
                                                                  <> ".self, forKey: .contents)"
                    , "            self = ." <> cn <> "(v0)"
                    ]
                  _ -> T.unlines [
                      "            var contentsContainer = try container.nestedUnkeyedContainer(forKey: .contents)"
                    , T.unlines $ zip [ 0 :: Int .. ] fs <&> \(i,(_,f)) -> do
                        "            let v" <> T.pack (show i)
                          <> " = try contentsContainer.decode(" <> f.typeName <> ".self)"
                    , "            self = ." <> cn <> "("
                        <> do T.intercalate "," $ zip [ 0 :: Int .. ] fs <&> \(i,_) -> do
                                "v" <> T.pack (show i)
                        <> do ")"
                    ]
              ]
          , "        }"
          , "    }"
          , ""
          , "    public func encode(to encoder: Encoder) throws {"
          , "        var container = encoder.container(keyedBy: Keys.self)"
          , "        switch self {"
          , T.unlines $ ss <&> \(cn, toList -> fs) -> case fs of
              [] -> T.unlines [
                   "        case ." <> cn <> ":"
                 , "            try container.encode(Tag." <> cn <> ", forKey: .tag)"
                 ]
              [ _ ] -> T.unlines [
                   "        case let ." <> cn <> "(v0):"
                 , "            try container.encode(Tag." <> cn <> ", forKey: .tag)"
                 , "            try container.encode(v0, forKey: .contents)"
                 ]
              _ -> T.unlines [
                   "        case let ." <> cn <> "("
                     <> do T.intercalate "," $ zip [ 0 :: Int .. ] fs <&> \(i,_) -> do
                             "v" <> T.pack (show i)
                     <> "):"
                 , "            try container.encode(Tag." <> cn <> ", forKey: .tag)"
                 , "            var contentsContainer = container.nestedUnkeyedContainer(forKey: .contents)"
                 , T.unlines $ zip [ 0 :: Int .. ] fs <&> \(i,_) -> do
                     "            try contentsContainer.encode(v" <> T.pack (show i) <> ")"
                 ]
          , "        }"
          , "    }"
          , ""
          , "    private enum Keys: String , CodingKey {"
          , "        case tag"
          , "        case contents"
          , "    }"
          , ""
          , "    private enum Tag : String , Codable {"
          , T.unlines $ ss <&> \(cn,_) -> "        case " <> cn
          , "    }"
          , "}"
          ]
  gjsEncodeSwift _ = Nothing
  gjsDecodeSwift _ = Nothing



class GSwiftProduct s f where
  rfieldsSwift :: DList (Maybe FieldName , FlatType)

instance (KnownSymbol n , ForeignType b) => GSwiftProduct s (S1 (MetaSel (Just n) x y z) (K1 i b)) where
  rfieldsSwift = pure $ (,) (Just (T.pack (symbolVal (Proxy :: Proxy n)))) do 
    FlatType do dependencies @b Swift
             do typeName @b Swift
             do jsEncode @b Swift
             do jsDecode @b Swift

instance ForeignType b => GSwiftProduct s (S1 (MetaSel Nothing x y z) (K1 i b)) where
  rfieldsSwift = pure $ (,) Nothing do 
    FlatType do dependencies @b Swift
             do typeName @b Swift
             do jsEncode @b Swift
             do jsDecode @b Swift

instance (GSwiftProduct s f , GSwiftProduct s g) => GSwiftProduct s (f :*: g) where
  rfieldsSwift = rfieldsSwift @s @f <> rfieldsSwift @s @g


instance GSwiftProduct s f => GSwiftProduct s (D1 x f) where
  rfieldsSwift = rfieldsSwift @s @f

instance GSwiftProduct s f => GSwiftProduct s (C1 (MetaCons x y z) f) where
  rfieldsSwift = rfieldsSwift @s @f



class GSwiftEnum s f where
  gEnumBodySwift :: DList (ConstructorName , DList (Maybe FieldName , FlatType))

instance {-# OVERLAPPING #-} KnownSymbol n => GSwiftEnum s (C1 (MetaCons n x y) U1) where
  gEnumBodySwift = pure (T.pack (symbolVal (Proxy :: Proxy n)) , mempty)

instance (KnownSymbol n , GSwiftProduct s f) => GSwiftEnum s (C1 (MetaCons n x y) f) where
  gEnumBodySwift = pure (T.pack (symbolVal (Proxy :: Proxy n)) , rfieldsSwift @s @f)

instance (GSwiftEnum s f , GSwiftEnum s g) => GSwiftEnum s (f :+: g) where
  gEnumBodySwift = gEnumBodySwift @s @f <> gEnumBodySwift @s @g

instance ForeignType Int8 where
  dependencies l@Dart = M.singleton (typeName @Int8 l) Nothing
  dependencies l@Swift = M.singleton (typeName @Int8 l) Nothing
  
  typeName Dart = "int"
  typeName Swift = "Int"
  
  declaration Dart = Nothing
  declaration Swift = Nothing
  
  jsEncode Dart = Nothing
  jsEncode Swift = Nothing
  
  jsDecode Dart = Nothing
  jsDecode Swift = Nothing

instance ForeignType Int16 where
  dependencies l@Dart = M.singleton (typeName @Int16 l) Nothing
  dependencies l@Swift = M.singleton (typeName @Int16 l) Nothing
  
  typeName Dart = "int"
  typeName Swift = "Int"
  
  declaration Dart = Nothing
  declaration Swift = Nothing
  
  jsEncode Dart = Nothing
  jsEncode Swift = Nothing
  
  jsDecode Dart = Nothing
  jsDecode Swift = Nothing

instance ForeignType Int32 where
  dependencies l@Dart = M.singleton (typeName @Int32 l) Nothing
  dependencies l@Swift = M.singleton (typeName @Int32 l) Nothing
  
  typeName Dart = "int"
  typeName Swift = "Int"
  
  declaration Dart = Nothing
  declaration Swift = Nothing
  
  jsEncode Dart = Nothing
  jsEncode Swift = Nothing
  
  jsDecode Dart = Nothing
  jsDecode Swift = Nothing

instance ForeignType Int64 where
  dependencies l@Dart = M.singleton (typeName @Int64 l) Nothing
  dependencies l@Swift = M.singleton (typeName @Int64 l) Nothing
  
  typeName Dart = "int"
  typeName Swift = "Int"
  
  declaration Dart = Nothing
  declaration Swift = Nothing
  
  jsEncode Dart = Nothing
  jsEncode Swift = Nothing
  
  jsDecode Dart = Nothing
  jsDecode Swift = Nothing

instance ForeignType Int where
  dependencies l@Dart = M.singleton (typeName @Int l) Nothing
  dependencies l@Swift = M.singleton (typeName @Int l) Nothing
  
  typeName Dart = "int"
  typeName Swift = "Int"
  
  declaration Dart = Nothing
  declaration Swift = Nothing
  
  jsEncode Dart = Nothing
  jsEncode Swift = Nothing
  
  jsDecode Dart = Nothing
  jsDecode Swift = Nothing

instance ForeignType Word8 where
  dependencies l@Dart = M.singleton (typeName @Word8 l) Nothing
  dependencies l@Swift = M.singleton (typeName @Word8 l) Nothing
  
  typeName Dart = "int"
  typeName Swift = "Int"
  
  declaration Dart = Nothing
  declaration Swift = Nothing
  
  jsEncode Dart = Nothing
  jsEncode Swift = Nothing
  
  jsDecode Dart = Nothing
  jsDecode Swift = Nothing

instance ForeignType Word16 where
  dependencies l@Dart = M.singleton (typeName @Word16 l) Nothing
  dependencies l@Swift = M.singleton (typeName @Word16 l) Nothing
  
  typeName Dart = "int"
  typeName Swift = "Int"
  
  declaration Dart = Nothing
  declaration Swift = Nothing
  
  jsEncode Dart = Nothing
  jsEncode Swift = Nothing
  
  jsDecode Dart = Nothing
  jsDecode Swift = Nothing

instance ForeignType Word32 where
  dependencies l@Dart = M.singleton (typeName @Word32 l) Nothing
  dependencies l@Swift = M.singleton (typeName @Word32 l) Nothing
  
  typeName Dart = "int"
  typeName Swift = "Int"
  
  declaration Dart = Nothing
  declaration Swift = Nothing
  
  jsEncode Dart = Nothing
  jsEncode Swift = Nothing
  
  jsDecode Dart = Nothing
  jsDecode Swift = Nothing

instance ForeignType Word64 where
  dependencies l@Dart = M.singleton (typeName @Word64 l) Nothing
  dependencies l@Swift = M.singleton (typeName @Word64 l) Nothing
  
  typeName Dart = "int"
  typeName Swift = "Int"
  
  declaration Dart = Nothing
  declaration Swift = Nothing
  
  jsEncode Dart = Nothing
  jsEncode Swift = Nothing
  
  jsDecode Dart = Nothing
  jsDecode Swift = Nothing

instance ForeignType Word where
  dependencies l@Dart = M.singleton (typeName @Word l) Nothing
  dependencies l@Swift = M.singleton (typeName @Word l) Nothing
  
  typeName Dart = "int"
  typeName Swift = "Int"
  
  declaration Dart = Nothing
  declaration Swift = Nothing
  
  jsEncode Dart = Nothing
  jsEncode Swift = Nothing
  
  jsDecode Dart = Nothing
  jsDecode Swift = Nothing

instance ForeignType Bool where
  dependencies l@Dart = M.singleton (typeName @Bool l) Nothing
  dependencies l@Swift = M.singleton (typeName @Bool l) Nothing
  
  typeName Dart = "bool"
  typeName Swift = "Bool"
  
  declaration Dart = Nothing
  declaration Swift = Nothing
  
  jsEncode Dart = Nothing
  jsEncode Swift = Nothing
  
  jsDecode Dart = Nothing
  jsDecode Swift = Nothing

instance ForeignType Text where
  dependencies l@Dart = M.singleton (typeName @Text l) Nothing
  dependencies l@Swift = M.singleton (typeName @Text l) Nothing
  
  typeName Dart = "String"
  typeName Swift = "String"
  
  declaration Dart = Nothing
  declaration Swift = Nothing
  
  jsEncode Dart = Nothing
  jsEncode Swift = Nothing
  
  jsDecode Dart = Nothing
  jsDecode Swift = Nothing

instance ForeignType a => ForeignType [ a ] where
  dependencies l@Dart = dependencies @a l
  dependencies l@Swift = dependencies @a l
  
  typeName Dart = "List<" <> typeName @a Dart <> ">"
  typeName Swift = "[ " <> typeName @a Swift <> " ]"
  
  declaration Dart = Nothing
  declaration Swift = Nothing
  
  jsEncode Dart = Just case jsEncode @a Dart of
    Just e ->
      "((xs) => xs.map((x) => " <> e <> "(x)).toList() as List<dynamic>)"
    Nothing -> "((xs) => xs as List<dynamic>)"
  jsEncode Swift = Nothing

  jsDecode Dart = Just case jsDecode @a Dart of
    Just d ->
      "((json) => (json as List<dynamic>).map((x) => " <> d <> "(x) as " <> typeName @a Dart <> ").toList())"
    Nothing -> "((json) => (json as List<dynamic>).map((x) => x as " <> typeName @a Dart <> ").toList())"
  jsDecode Swift = Nothing

instance ForeignType a => ForeignType (Maybe a) where
  dependencies l@Dart = dependencies @a l
  dependencies l@Swift = dependencies @a l
  
  typeName Dart = typeName @a Dart <> "?"
  typeName Swift = typeName @a Swift <> "?"
  
  declaration Dart = Nothing
  declaration Swift = Nothing
  
  jsEncode Dart = Just case jsEncode @a Dart of
    Just e -> "((x) => x == null ? null : " <> e <> "(x!))"
    Nothing -> "((x) => x == null ? null : x!)"
  jsEncode Swift = Nothing
  
  jsDecode Dart = Just case jsDecode @a Dart of
    Just d -> "((json) => json == null ? null : " <> d <> "(json!))"
    Nothing -> "((json) => json == null ? null : json!)"
  jsDecode Swift = Nothing

instance (ForeignType a , ForeignType b) => ForeignType (Either a b) where
  dependencies l@Dart = M.insert do "Either<T,U>"
                                 do declaration @(Either a b) l
                                 do dependencies @a l <> dependencies @b l
  dependencies l@Swift = M.insert do "Either<T,U>"
                                  do declaration @(Either a b) l
                                  do dependencies @a l <> dependencies @b l

  typeName Dart = "Either<" <> typeName @a Dart <> "," <> typeName @b Dart <> ">"
  typeName Swift = "Either<" <> typeName @a Swift <> "," <> typeName @b Swift <> ">"


  declaration Dart = Just $ T.concat [
       "class Either<T,U> {\n"
     , "    // constructors\n"
     , "    Either.Left(T x):_lr=false,_l=x,_r=null;\n"
     , "    Either.Right(U x):_lr=true,_l=null,_r=x;\n"
     , "    \n"
     , "    // pattern match, guaranteed to call exactly 1 of the two provided callbacks\n"
     , "    match<Z>(Z Function(T) l , Z Function(U) r) {\n"
     , "        if(_lr){return l(_l!);}else{return r(_r!);}\n"
     , "    }\n"
     , "    \n"
     , "    // private internals\n"
     , "    final bool _lr;\n"
     , "    final T? _l;\n"
     , "    final U? _r;\n"
     , "    Either._(U x):_lr=true,_l=null,_r=x;\n"
     , "}"
     ]
  declaration Swift = Just $ T.concat [
       "public enum Either< T , U > {\n"
     , "    case Left(T)\n"
     , "    case Right(U)\n"
     , "}\n"
     , "\n"
     , "extension Either : Equatable where T : Equatable , U : Equatable {}\n"
     , "extension Either : Hashable where T : Hashable , U : Hashable {}\n"
     , "extension Either : Codable where T : Codable , U : Codable {\n"
     , "\n"
     , "    public init(from decoder: Decoder) throws {\n"
     , "        let container = try decoder.container(keyedBy: Keys.self)\n"
     , "\n"
     , "        do {\n"
     , "            let x = try container.decode(T.self, forKey: .Left)\n"
     , "            self = .Left(x)\n"
     , "        }catch(_){\n"
     , "            let x = try container.decode(U.self, forKey: .Right)\n"
     , "            self = .Right(x)\n"
     , "        }\n"
     , "    }\n"
     , "\n"
     , "    public func encode(to encoder: Encoder) throws {\n"
     , "        var container = encoder.container(keyedBy: Keys.self)\n"
     , "        switch self {\n"
     , "        case let .Left(a):\n"
     , "            try container.encode(a, forKey: .Left)\n"
     , "        case let .Right(a):\n"
     , "            try container.encode(a, forKey: .Right)\n"
     , "        }\n"
     , "    }\n"
     , "\n"
     , "    private enum Keys: String, CodingKey {\n"
     , "        case Left , Right\n"
     , "    }\n"
     , "}"
    ]

  jsEncode Dart = Just $ "((x)=>x.match(((l) => {\"Left\":"
     <> do fromMaybe "" (jsEncode @a Dart)
     <> do "(l)}),"
     <> do "((r) => {\"Right\":" <> fromMaybe "" (jsEncode @b Dart) <> "(r)})"
     <> do "))"
  jsEncode Swift = Nothing

  jsDecode Dart = Just $ "((x) => x[\"Left\"] ? "
     <> do fromMaybe "" (jsEncode @a Dart) <> "(x[\"Left\"])"
     <> do ":"
     <> do fromMaybe "" (jsEncode @b Dart) <> "(x[\"Right\"])"
     <> do ")"
  jsDecode Swift = Nothing

instance ForeignType a => ForeignType (Identity a) where
  dependencies = dependencies @a
  typeName = typeName @a
  declaration = declaration @a
  jsEncode = jsEncode @a
  jsDecode = jsDecode @a

instance ForeignType a => ForeignType (Const a b) where
  dependencies = dependencies @a
  typeName = typeName @a
  declaration = declaration @a
  jsEncode = jsEncode @a
  jsDecode = jsDecode @a



-- dart: ideally we'd use the empty tuple here, but tuples are currently only on the dev branch of dart
-- swift: again, ideally we'd use the empty tuple, but Swift does not have a Codable instance and we are forbidden to extend it ourselves
instance ForeignType () where
  dependencies l@Dart = M.singleton (typeName @() l) (declaration @() l)
  dependencies l@Swift = M.singleton (typeName @() l) (declaration @() l)

  typeName Dart = "Top"
  typeName Swift = "Top"

  declaration Dart = Just "enum Top {\n  Trivial\n}"
  declaration Swift = Just "enum Top : Codable , Hashable {\n    case Trivial\n    \n    public init(from decoder: Decoder) throws {\n        self = .Trivial\n    }\n\n    public func encode(to encoder: Encoder) throws {\n        let _ = encoder.unkeyedContainer()\n    }\n}"

  jsEncode Dart = Just "((_) => [])"
  jsEncode Swift = Nothing

  jsDecode Dart = Just "((_) => Top.Trivial)"
  jsDecode Swift = Nothing

instance (ForeignType a , ForeignType b) => ForeignType (a , b)
instance (ForeignType a , ForeignType b , ForeignType c) => ForeignType (a , b , c)
instance (ForeignType a , ForeignType b , ForeignType c , ForeignType d) => ForeignType (a , b , c , d)
instance (ForeignType a , ForeignType b , ForeignType c , ForeignType d , ForeignType e) => ForeignType (a , b , c , d , e)



